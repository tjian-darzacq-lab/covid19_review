# Overcoming the bottleneck to widespread testing: A rapid review of nucleic acid testing approaches for COVID-19 detection
Meagan N. Esbin, Oscar N. Whitney, Shasha Chong, Anna Maurer, Xavier Darzacq, Robert Tjian

COVID-19 is a global health crisis and accurate, rapid testing needs to increase by orders of magnitude to measure the scope of the disease and monitor population outbreaks before they spread. This review summarizes recent advances in nucleic acid testing for the detection of COVID-19. Both preprint and published articles are reviewed and their methods, costs, workflow time, and limit of detection are quantitatively compared. We hope to rapidly disseminate information to relevant researchers wishing to optimize and massively scale up COVID-19 testing during this global pandemic. 

Preprint DOI: [![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.3776183.svg)](https://doi.org/10.5281/zenodo.3776183) 

## Published online at RNA, May 1st, 2020

Link: https://rnajournal.cshlp.org/content/early/2020/05/01/rna.076232.120 \
DOI: 10.1261/rna.076232.120